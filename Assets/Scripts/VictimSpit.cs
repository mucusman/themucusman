﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictimSpit : MonoBehaviour {
    public float m_SpitDownTime =3f;
    private int m_SpitAmmount = 12;
    public Transform m_Mouth;
    public GameObject spitPrefab;
    public GameObject spitPrefab2;
    public GameObject spitPrefab3;
    bool m_IsInfected;

    private bool leftClick;
    private bool isActioning = false;
    public GameObject m_VictimAsset;
    
    void Start()
    {
        m_IsInfected = false;
        
    }
    
    public void Infected()
    {
        if (!m_IsInfected)
        {
            InvokeRepeating("Spit", m_SpitDownTime, m_SpitDownTime);
            m_IsInfected = true;
        }
        
    }

    private void Spit()
    {
        StartCoroutine(Spitting());
    }

    IEnumerator Spitting()
    {
        for (int i = 0; i < m_SpitAmmount; i++)
        {
            int random = Random.Range(0, 3);
            GameObject prefabToSpawn;
            switch (random)
            {
                case 2:
                    prefabToSpawn = spitPrefab3;
                    break;
                case 1:
                    prefabToSpawn = spitPrefab2;
                    break;
                case 0:
                    prefabToSpawn = spitPrefab;
                    break;
                default:
                    prefabToSpawn = spitPrefab;
                    break;
            }
            GameObject spit = Instantiate(prefabToSpawn, transform.Find("Mouth").transform.position, Quaternion.identity);
            spit.GetComponent<Spit>().SetSpitId(gameObject.GetInstanceID());
            spit.GetComponent<Rigidbody>().velocity = transform.GetChild(0).transform.forward * 30f;
            spit.GetComponent<Rigidbody>().velocity += new Vector3(0, 4f, 0);
            yield return new WaitForSeconds(0.1f);
        }
        isActioning = false;
    }
}
