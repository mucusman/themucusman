﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillGenerator : MonoBehaviour {

    public int pillsOnMap = 0;
    public GameObject pillPrefab;
    public List<Transform> m_SpawnPoints;

    // Use this for initialization
    void Start () {
        InvokeRepeating("SpawnPillsOnMap", 0, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void SpawnPillsOnMap()
    {

        if (pillsOnMap < 2)
        {
            int index = Random.Range(0, m_SpawnPoints.Count);
            Collider[] hitColliders = Physics.OverlapSphere(m_SpawnPoints[index].position, 0.8f);
            if (hitColliders.Length == 0)
            {
                Debug.Log(index);
                GameObject pillGO = Instantiate(pillPrefab, m_SpawnPoints[index].position, m_SpawnPoints[index].rotation);
                pillsOnMap += 1;
            }

        }
    }
}
