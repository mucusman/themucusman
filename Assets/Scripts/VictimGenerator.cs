﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictimGenerator : MonoBehaviour {

    public List<Transform> m_SpawnPoints;
    public GameObject m_VictimPrefab;
    public int levelnumber;


	void Start () {
        levelnumber = 0;

		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CreateVictim(int index)
    {
        //int i = Random.Range(0, m_SpawnPoints.Count-1);
        GameObject newVictim = Instantiate(m_VictimPrefab, m_SpawnPoints[index].position, m_SpawnPoints[index].rotation);
        newVictim.GetComponent<AIPath>().SetNewDestination();

    }

    public void StartNewLevel(int level)
    {
        GameObject[] Victims = GameObject.FindGameObjectsWithTag("Victim");
        for(int i = 0; i < Victims.Length; i++)
        {
            Destroy(Victims[i]);
        }

        for (int i = 0; i < 25+(level*5); i++)
        {
            CreateVictim(i % 25);
        }
    }
}
