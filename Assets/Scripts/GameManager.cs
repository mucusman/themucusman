﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour {
    public GameObject m_Player;
    public GameObject m_GameOverPanel;

    public GameObject m_CongratulationPanel;
    public int Level;

    public Animator cameraAnimator;
    private bool throwUp = false;

    private AudioSource source;
    public AudioClip gameMusic;

    void Start () {
        Level = 0;
        StartNewLevel(0);
        m_GameOverPanel.SetActive(false);

        source = GetComponent<AudioSource>();
        InvokeRepeating("PlayBackgroundMusic", 0, 50.526f);

	}

    private void PlayBackgroundMusic()
    {
        source.PlayOneShot(gameMusic);
    }

    public void GameOver()
    {
        GameObject.Find("FPSController").GetComponent<FirstPersonController>().enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        m_GameOverPanel.SetActive(true);

        // Player throw up
        
    }

    

    public void TryAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LevelComplete()
    {
        GameObject.Find("FPSController").GetComponent<FirstPersonController>().enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        m_CongratulationPanel.SetActive(true);
    }

    public void NextLevelButtonTarget()
    {
        Level++;
        StartNewLevel(Level);
        
    }

    public void StartNewLevel(int level)
    {
        GameObject.Find("FPSController").GetComponent<FirstPersonController>().enabled = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = false;
        m_GameOverPanel.SetActive(false);
        m_CongratulationPanel.SetActive(false);
        GetComponent<HealthManager>().StartNewLevel();
        GetComponent<ScoreTally>().StartNewLevel(level);
        GetComponent<AgentGenerator>().StartNewLevel(level);
        GetComponent<VictimGenerator>().StartNewLevel(level);

    }
}
