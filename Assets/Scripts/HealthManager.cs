﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    public Image m_Health;
    public float m_PoliceDamage=0.2f;

    public float m_CurrentHealth;
    public float m_Speed;
    public float m_HealthPack = 0.2f;
    public int m_DownTime=3;
    public bool m_HasHit;
    bool InCoroutine;
    public ScreenShake screenShake;
    bool m_LevelComplete;
    public float HealthDuration= 240;
    

    void Start()
    {
        StartNewLevel();
        InvokeRepeating("HealthUpdate", 0, 0.1f);
        m_Speed = 0.1f/HealthDuration ;
        m_LevelComplete = false;
    }

    void HealthUpdate()
    {
        if (!m_LevelComplete)
        {
            if (m_HasHit)
            {
                if (!InCoroutine)
                {
                    InCoroutine = true;
                    StartCoroutine("HitTarget");
                }

            }
            else
            {
                m_CurrentHealth -= m_Speed;
                m_Health.fillAmount = m_CurrentHealth;
                if (m_Health.fillAmount == 0)
                {
                    GetComponent<GameManager>().GameOver();
                }
            }
        }
        
    }

    private IEnumerator HitTarget()
    {
        yield return new WaitForSeconds(m_DownTime);
        m_HasHit = false;
        InCoroutine = false;
    }

    public void HealthPickUp()
    {
        m_CurrentHealth += m_HealthPack;
        if (m_CurrentHealth > 1f)
        {
            m_CurrentHealth = 1f;
        }
        m_Health.fillAmount = m_CurrentHealth;
    }

    public void TargetHit()
    {
        m_HasHit = true;
    }

    public void PoliceHit()
    {
        m_CurrentHealth -= m_PoliceDamage;
        screenShake.shakeDuration = 0.7f;
        if (m_CurrentHealth <0)
        {
            GetComponent<GameManager>().GameOver();
        }
        m_Health.fillAmount = m_CurrentHealth;
    }

    public void LevelComplete()
    {
        m_LevelComplete = true;
    }

    public void StartNewLevel()
    {
        m_CurrentHealth = 1f;
        m_Health.fillAmount = m_CurrentHealth;
        m_HasHit = false;
        InCoroutine = false;
        m_LevelComplete = false;
    }
}
