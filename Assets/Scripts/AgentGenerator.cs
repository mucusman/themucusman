﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentGenerator : MonoBehaviour {
    public List<Transform> m_SpawnPoints;
    public GameObject m_AgentPrefab;
    // Use this for initialization
    public int levelnumber;


    void Start()
    {
        levelnumber = 0;


    }

    public void CreateAgent()
    {
        int i = Random.Range(0, m_SpawnPoints.Count-1);
        GameObject newVictim = Instantiate(m_AgentPrefab, m_SpawnPoints[i].position, m_SpawnPoints[i].rotation);
        //newVictim.GetComponent<AIPath>().SetNewDestination();

    }

    public void StartNewLevel(int level)
    {
        GameObject[] Agents = GameObject.FindGameObjectsWithTag("Police");
        for (int i = 0; i < Agents.Length; i++)
        {
            Destroy(Agents[i]);
        }

        for (int i = 0; i < level; i++)
        {
            CreateAgent();
        }
    }
}
