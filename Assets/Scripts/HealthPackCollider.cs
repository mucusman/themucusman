﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPackCollider : MonoBehaviour {
    HealthManager HealthManagerScript;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }
}
