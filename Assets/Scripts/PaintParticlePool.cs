﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintParticlePool : MonoBehaviour
{

    public int maxDecals = 100;
    public float decalSizeMin = .5f;
    public float decalSizeMax = 1.5f;

    private ParticleSystem decalParticleSystem;
    private int particleDecalDataIndex;
    private PaintParticleData[] particleData;
    private ParticleSystem.Particle[] particles;

    void Start()
    {
        decalParticleSystem = GetComponent<ParticleSystem>();
        particles = new ParticleSystem.Particle[maxDecals];
        particleData = new PaintParticleData[maxDecals];
        for (int i = 0; i < maxDecals; i++)
        {
            particleData[i] = new PaintParticleData();
        }
    }

    public void ParticleHit(Collision coll, Gradient colorGradient)
    {
        SetParticleData(coll, colorGradient);
        DisplayParticles();
    }

    void SetParticleData(Collision coll, Gradient colorGradient)
    {
        if (particleDecalDataIndex >= maxDecals)
        {
            particleDecalDataIndex = 0;
        }
        
        particleData[particleDecalDataIndex].position = coll.contacts[0].point + (coll.contacts[0].normal * 0.001f);
        Vector3 particleRotationEuler = Quaternion.LookRotation(-coll.contacts[0].normal).eulerAngles;
        particleRotationEuler.z = Random.Range(0, 360);
        particleData[particleDecalDataIndex].rotation = particleRotationEuler;
        particleData[particleDecalDataIndex].size = Random.Range(decalSizeMin, decalSizeMax);
        //particleData[particleDecalDataIndex].color = colorGradient.Evaluate(Random.Range(0f, 1f));

        particleDecalDataIndex++;
    }

    void DisplayParticles()
    {
        for (int i = 0; i < particleData.Length; i++)
        {
            particles[i].position = particleData[i].position;
            particles[i].rotation3D = particleData[i].rotation;
            particles[i].startSize = particleData[i].size;
            //particles[i].startColor = particleData[i].color;
        }
        
        decalParticleSystem.SetParticles(particles, particles.Length);
    }
}