﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public GameObject spitPrefab;
    public GameObject spitPrefab2;
    public GameObject spitPrefab3;
    public GameObject spitPrefab4;
    private AudioSource source;
    public AudioClip[] spitSounds;
    public AudioClip[] pillSounds;

    private bool leftClick;
    private bool isActioning = false;
    private Rigidbody body;

	void Start () {
        body = GetComponent<Rigidbody>();
        source = GetComponent<AudioSource>();
    }
	
	void Update () {
        ResetKeys();
        if (Input.GetMouseButton(0))
        {
            leftClick = true;
        }
    }


    private void FixedUpdate()
    {
        if (leftClick && !isActioning) {
            isActioning = true;
            int random = Random.Range(0, spitSounds.Length);
            source.PlayOneShot(spitSounds[random]);
            StartCoroutine(Spit());
        }
    }

    private void ResetKeys()
    {
        leftClick = false;
    }

    public void PlayPills()
    {
        int random = Random.Range(0, pillSounds.Length);
        source.PlayOneShot(pillSounds[random]);
    }

    IEnumerator Spit()
    {
        for (int i = 0; i < 5; i++)
        {
            int random = Random.Range(0, 4);
            GameObject prefabToSpawn;
            switch (random)
            {
                case 3:
                    prefabToSpawn = spitPrefab4;
                    break;
                case 2:
                    prefabToSpawn = spitPrefab3;
                    break;
                case 1:
                    prefabToSpawn = spitPrefab2;
                    break;
                case 0:
                    prefabToSpawn = spitPrefab;
                    break;
                default:
                    prefabToSpawn = spitPrefab;
                    break;
            }
            GameObject spit = Instantiate(prefabToSpawn, transform.Find("Mouth").transform.position, Quaternion.identity);
            GameObject spit2 = Instantiate(prefabToSpawn, transform.Find("Mouth").transform.position, Quaternion.identity);
            GameObject spit3 = Instantiate(prefabToSpawn, transform.Find("Mouth").transform.position, Quaternion.identity);
            Vector3 offset1 = new Vector3(Random.Range(-3f, 3f), Random.Range(-2f, 2f), Random.Range(-3f, 3f));
            Vector3 offset2 = new Vector3(Random.Range(-3f, 3f), Random.Range(-2f, 2f), Random.Range(-3f, 3f));
            Vector3 offset3 = new Vector3(Random.Range(-3f, 3f), Random.Range(-2f, 2f), Random.Range(-3f, 3f));
            spit.GetComponent<Rigidbody>().velocity = transform.GetChild(0).transform.forward * 30f;
            spit.GetComponent<Rigidbody>().velocity += new Vector3(0, 4f, 0);
            spit.GetComponent<Rigidbody>().velocity += offset1;
            spit2.GetComponent<Rigidbody>().velocity = transform.GetChild(0).transform.forward * 30f;
            spit2.GetComponent<Rigidbody>().velocity += new Vector3(0, 4f, 0);
            spit2.GetComponent<Rigidbody>().velocity += offset2;
            spit3.GetComponent<Rigidbody>().velocity = transform.GetChild(0).transform.forward * 30f;
            spit3.GetComponent<Rigidbody>().velocity += new Vector3(0, 4f, 0);
            spit3.GetComponent<Rigidbody>().velocity += offset3;
            yield return new WaitForSeconds(0.1f);
        }
        isActioning = false;
    }
}
