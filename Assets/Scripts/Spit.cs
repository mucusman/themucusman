﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spit : MonoBehaviour {

    private PaintParticlePool paintParticlePool;
    private PaintParticlePool paintParticlePool2;
    private PaintParticlePool paintParticlePool3;
    private PaintParticlePool paintParticlePool4;
    private Rigidbody body;
    public Gradient particleColorGradient;
    private int id = 0;

    // Use this for initialization
    void Start () {
        paintParticlePool = GameObject.Find("PaintParticleSystem").GetComponent<PaintParticlePool>();
        paintParticlePool2 = GameObject.Find("PaintParticleSystem2").GetComponent<PaintParticlePool>();
        paintParticlePool3 = GameObject.Find("PaintParticleSystem3").GetComponent<PaintParticlePool>();
        paintParticlePool4 = GameObject.Find("PaintParticleSystem4").GetComponent<PaintParticlePool>();
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate () {
        transform.rotation = Quaternion.LookRotation(body.velocity);
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "BuildingWalls")
        {
            int random = Random.Range(0, 4);
            switch (random)
            {
                case 3:
                    paintParticlePool4.ParticleHit(coll, particleColorGradient);
                    break;
                case 2:
                    paintParticlePool3.ParticleHit(coll, particleColorGradient);
                    break;
                case 1:
                    paintParticlePool2.ParticleHit(coll, particleColorGradient);
                    break;
                case 0:
                    paintParticlePool.ParticleHit(coll, particleColorGradient);
                    break;
                default:
                    paintParticlePool.ParticleHit(coll, particleColorGradient);
                    break;
            }
        }
       
        Destroy(gameObject);
    }

    public void SetSpitId(int id)
    {
        this.id = id;
    }

    public int GetSpitId()
    {
        return this.id;
    }
}
