﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentCollider : MonoBehaviour
{
    public float m_HitDownTime = 3f;
    bool m_HitByProjectile;
    public GameObject m_AgentAsset;
    public Color[] m_ColorLerp;
    float ColorLerp;
    public AudioClip[] sounds;

    private AudioSource source;

    void OnTriggerEnter(Collider collision)
    {
        if (!m_HitByProjectile&& collision.gameObject.CompareTag("Projectile"))
        {
            m_HitByProjectile = true;
            GetComponent<IAstarAI>().canMove = false;
            m_AgentAsset.GetComponent<Renderer>().material.color = m_ColorLerp[1];
            //ColorLerp = 0;
            StartCoroutine("DownTime");
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            //GetComponent<AIPathAgent>().PlayerContact();
            m_HitByProjectile = true;
            GetComponent<IAstarAI>().canMove = false;
            m_AgentAsset.GetComponent<Renderer>().material.color = m_ColorLerp[1];
            //ColorLerp = 0;
            StartCoroutine("DownTime");
        }
        
    }

        private void Start()
    {
        source = GetComponent<AudioSource>();
        InvokeRepeating("PlaySiren", 1, 5f);
        m_HitByProjectile = false;
    }

    private void PlaySiren()
    {
        int random = Random.Range(0, sounds.Length);
        //source.clip = sounds[random];
        //source.Play();
        source.PlayOneShot(sounds[random]);
    }
    

    private IEnumerator DownTime()
    {
        yield return new WaitForSeconds(m_HitDownTime);
        m_HitByProjectile = false;
        GetComponent<IAstarAI>().canMove = true;
        m_AgentAsset.GetComponent<Renderer>().material.color = m_ColorLerp[0];

    }



}
