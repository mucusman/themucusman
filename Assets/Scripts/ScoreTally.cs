﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTally : MonoBehaviour {

    public Text m_Score;
    public int m_LevelTarget;
    public int m_Current;
	// Update is called once per frame

    void Start()
    {
        m_Current = 0;
    }
	void Update () {

        m_Score.text = "Infected: " + m_Current + "/" + m_LevelTarget;
        if (m_Current >= m_LevelTarget)
        {
            LevelComplete();
        }
	}

    public void StartNewLevel(int level)
    {
        m_LevelTarget = 10 + (level * 5);
        m_Current = 0;
    }

    public void LevelComplete()
    {
        GetComponent<GameManager>().LevelComplete();
    }
}
