﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerCollider : MonoBehaviour
{
    HealthManager HealthManagerScript;
    public float m_PoliceDownTime = 3f;
    bool m_JustHit;
    float mass = 3.0F; // defines the character mass
    Vector3 impact = Vector3.zero;
    CharacterController character;
    void Start()
    {
        HealthManagerScript = GameObject.Find("GameManager").GetComponent<HealthManager>();
        m_JustHit = false;

        StartCoroutine("GetControllerScript");
    }

    IEnumerator GetControllerScript()
    {
        yield return new WaitForSeconds(1);
        character = GetComponent<FirstPersonController>().m_CharacterController;
    }


        void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("HealthPack"))
        {
            HealthManagerScript.HealthPickUp();
        }
        if (collision.gameObject.CompareTag("Police"))
        {
            AddImpact(-collision.transform.position + transform.position, 60);
            HealthManagerScript.PoliceHit();

            //if (!m_JustHit)
            //{
            //    AddImpact(-collision.transform.position+transform.position, 60);
            //    HealthManagerScript.PoliceHit();
            //    m_JustHit = true;
            //    StartCoroutine("AgentHit");
            //}

        }

    }

    IEnumerator AgentHit()
    {
        yield return new WaitForSeconds(m_PoliceDownTime);
        m_JustHit = false;
    }

    void Update()
    {
        // apply the impact force:
        if (impact.magnitude > 0.2F) character.Move(impact * Time.deltaTime);
        // consumes the impact energy each cycle:
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);
    }
    // call this function to add an impact force:
    public void AddImpact(Vector3 dir, float force)
    {
        dir.Normalize();
        if (dir.y < 0) dir.y = -dir.y; // reflect down force on the ground
        impact += dir.normalized * force / mass;
    }
    
}
