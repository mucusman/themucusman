﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pill : MonoBehaviour {

    private PillGenerator pillGenerator;
    private float offset;


    // Use this for initialization
    void Start() {
        pillGenerator = GameObject.Find("GameManager").GetComponent<PillGenerator>();
    }

    // Update is called once per frame
    void FixedUpdate() {
        offset = 0.03f * Mathf.Sin(Time.time * 4f);
        transform.position = new Vector3(transform.position.x, transform.position.y + offset, transform.position.z);
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            coll.gameObject.GetComponent<Player>().PlayPills();
            pillGenerator.pillsOnMap -= 1;
            Destroy(gameObject);
        }
        
    }
}