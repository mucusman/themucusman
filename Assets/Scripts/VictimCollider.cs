﻿using Pathfinding;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictimCollider : MonoBehaviour {
    public GameObject m_VictimAsset;
    public float m_NonHitDownTimeSeconds;
    public bool m_InDownTime;
    int m_HitNumber;
    public Color[] m_InfectedColor;
    bool m_FullyInfected;
    HealthManager HealthManagerScript;
    ScoreTally ScoreTallyScript;
    private AudioSource source;
    public AudioClip[] sounds;
    public Animator m_Animation;

    void Start()
    {
        m_Animation.enabled = true;
        m_HitNumber = 0;
        m_FullyInfected = false;
        m_InDownTime = false;
        HealthManagerScript = GameObject.Find("GameManager").GetComponent<HealthManager>();
        ScoreTallyScript = GameObject.Find("GameManager").GetComponent<ScoreTally>();
        source = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision);
        if (collision.gameObject.CompareTag("Projectile") && collision.gameObject.GetComponent<Spit>().GetSpitId() != gameObject.GetInstanceID())
        {
            if (!m_FullyInfected)
            {
                if (!m_InDownTime)
                {
                    int random = UnityEngine.Random.Range(0, sounds.Length);
                    source.PlayOneShot(sounds[random]);
                    PlayAnimation();
                    Debug.Log("hit victim");
                    HealthManagerScript.TargetHit();
                    m_VictimAsset.GetComponent<Renderer>().material.color = m_InfectedColor[m_HitNumber];
                    GetComponent<VictimSpit>().Infected();
                    m_HitNumber++;
                    if (m_HitNumber == m_InfectedColor.Length)
                    {
                        m_FullyInfected = true;
                        ScoreTallyScript.m_Current++;
                    }
                    else
                    {
                        m_InDownTime = true;
                        StartCoroutine("DownTime");
                    }
                }
                
            }
            
        }
        //if (collision.relativeVelocity.magnitude > 2)
        
    }

    private void PlayAnimation()
    {
        GetComponent<AIPath>().canMove = false;
        m_Animation.SetBool("Hit", true);
        StartCoroutine("AnimationDownTime");
    }

    private IEnumerator AnimationDownTime()
    {
        yield return new WaitForSeconds(6.26f);
        m_Animation.SetBool("Hit", false);
        GetComponent<AIPath>().canMove = true;
    }

        private IEnumerator DownTime()
    {
        yield return new WaitForSeconds(m_NonHitDownTimeSeconds);
        m_InDownTime = false;
    }
}
