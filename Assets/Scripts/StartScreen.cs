﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour {

    public string sceneName;
    public AudioClip titleSound;
    private AudioSource source;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
        if (!source.isPlaying)
        {
            source.PlayOneShot(titleSound, 1);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Begin()
    {
        SceneManager.LoadScene(sceneName);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
